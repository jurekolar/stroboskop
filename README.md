# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://jurekolar@bitbucket.org/jurekolar/stroboskop.git
git add --all
git commit -m "Inital commit"
git push origin master
```

Naloga 6.2.3:
https://bitbucket.org/jurekolar/stroboskop/commits/55ab5e04386d40375832fefbfa597827f62bdbad

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/jurekolar/stroboskop/commits/01f9b4d778ed9615d37b0e7afea287c55198641d

Naloga 6.3.2:
https://bitbucket.org/jurekolar/stroboskop/commits/e4c72ab0ae115c8b400ea0a647f44eb550681db0

Naloga 6.3.3:
https://bitbucket.org/jurekolar/stroboskop/commits/44d9cf1c745f944457280f0307cd0feacbaefc0a

Naloga 6.3.4:
https://bitbucket.org/jurekolar/stroboskop/commits/a20128f4d072b7154c5fc7f4e11eb5f8be3f4e7d

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/jurekolar/stroboskop/commits/fcef0774c4d119e22f038d5b43912fcd2467790a

Naloga 6.4.2:
https://bitbucket.org/jurekolar/stroboskop/commits/b295c7602ad8598f22165aaedf4684ada23878ff

Naloga 6.4.3:
https://bitbucket.org/jurekolar/stroboskop/commits/89247824c99055a10a949d4e2dfde783a6840671

Naloga 6.4.4:
https://bitbucket.org/jurekolar/stroboskop/commits/4b4dc2f249c50a7d3ef6af6ad9fd70a8cd9fe3d6