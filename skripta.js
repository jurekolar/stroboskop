window.addEventListener('load', function() {
	//stran nalozena
	
	//Dodaj novo barvo
	var dodajBarvo = function(event) {
		var input = document.createElement('button');
        var picker = new jscolor(input);
        picker.fromRGB(Math.floor(Math.random()*255), Math.floor(Math.random()*255), Math.floor(Math.random()*255))
        document.getElementById("barve").appendChild(input);
	}
	
	//klic funckije ob kliku na "novaBarva"
	document.querySelector("#novaBarva") 
		.addEventListener('click', dodajBarvo);
		
	//Odstrani barve
	var odstraniBarve = function(event) {
        document.getElementById("barve").innerHTML = "";
	}
	
	//klic funkcije ob kliku na "odstraniBarve"
	document.querySelector("#odstraniBarve") 
		.addEventListener('click', odstraniBarve);
	
	//Stroboskop
	var vrednosti = [];
	
	//privzete vrednosti intervalov
	var minCas = 1000;
	var maxCas = 1000;
	var ustavi = false;
	
	var spremeniBarvo = function(id) {
		document.getElementById("stroboskop").style.backgroundColor = "#"+vrednosti[id];

		if (ustavi) {
			ustavi = false;
		} else {
			novId = (id+1) % vrednosti.length;
			timeout = Math.floor((Math.random() * (maxCas - minCas + 1)) + minCas);
			//console.log(timeout);
			setTimeout(function() {spremeniBarvo(novId)} , timeout);
		}		
	}
	
	var stop = function(event) {
		ustavi = true;
		
		//preimenovanje in ponastavitev gumba Ustavi -> Zaženi
		var restart = document.querySelector("#start");
		restart.innerHTML = "Zaženi stroboskop";
		restart.removeEventListener('click', stop);
		restart.addEventListener('click', zagon);
	}
	
	var zagon = function(event) {
		vrednosti = [];
		var barve = document.querySelectorAll("#barve > button");
		for (i = 0; i < barve.length; i++) {
			var barva = barve[i];
			vrednosti.push(barva.innerHTML);
		}
		
		//prebere in nastavi Min in Max čas iz vnosnih polj
		minCas = document.getElementById('min').value;
		maxCas = document.getElementById('max').value;
		
		//za namen preverjanja
		//console.log(minCas);
		//console.log(maxCas);
		
		// odvečna stara koda
		/*
		minCas = 1000;
		maxCas = 1000;
		*/
		
		spremeniBarvo(0);
		
		var start = document.querySelector("#start");
		start.innerHTML = "Ustavi stroboskop";
		start.removeEventListener('click', zagon);
		start.addEventListener('click', stop);
	}
	
	document.querySelector("#start").addEventListener('click', zagon);
		
});